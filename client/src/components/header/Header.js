import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import IconButton from 'material-ui/IconButton';

import './styles.less';

const logo = require('./../../assets/images/logo.svg'); // eslint-disable-line

const propTypes = {
  clearStore: PropTypes.func,
  isInstalling: PropTypes.bool,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  logOut: PropTypes.func,
};

class Header extends PureComponent {
  render() {
    return (
      <div className="main-header flex between">
        <div className={'flex center'}>
          <IconButton containerElement={<Link to={'/'}/>}>
            <img src={logo}/>
          </IconButton>
        </div>
      </div>
    );
  }
}

Header.propTypes = propTypes;

export default withRouter(connect()(Header));
