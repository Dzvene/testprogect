export const getStarted = {
  title: 'Cluster name',
  description: 'Enter a name for your cluster. Use 4 to 10 alphanumeric characters. ',
  enterClusterName: 'Enter cluster name',
};

export const cloudProvider = {
  title: 'Cloud provider',
  description: 'Select a cloud service provider for your cluster.',
  selectCloudProvider: 'Select a cloud provider',
  selectCloudProviderVersion: 'Select a cloud provider version',
};

export const authorization = {
  title: 'Authorization',
  description: 'Enter the necessary credentials to access your cloud solution.\n',
  username: 'Username',
  projectName: 'Project name',
  password: 'Password',
};

export const clusterSpec = {
  title: 'Cluster specifications',
  description: 'Enter a number of nodes you want to have in your cluster (must be more than 1) and select the node type' +
  ' as specified by your cloud service provider. Copy and paste your public key and click Next.',
  numberNodes: 'Number of nodes',
  nodeType: 'Node type',
  publicKey: 'Public key',
};

export const solutionBlueprint = {
  title: 'Solution blueprint',
  description: 'Select a blueprint to install a pre-configured stack of microservices for your IoT case.',
  selectBlueprint: 'Select a blueprint',
};

export const summaryInformation = {
  title: 'Summary information',
  description:
  'Prior to installing the cluster, make sure you provided the right information.\n' +
  'If you want to edit the information entered, click Back. If everything looks good, click Next to start the cluster' +
  ' installation process.',
};

export const installation = {
  title: 'Installation',
  description: 'This may take several minutes.',
};

export const button = {
  confirm: 'Confirm',
  check: 'Check',
  next: 'Next',
  prev: 'Back',
  finish: 'Finish',
  startOver: 'Start Over',
  retryInstall: 'Retry installation',
  newCluster: 'New',
  cancel: 'Cancel',
  save: 'Save',
  deleteCluster: 'Delete cluster',
  logIn: 'Log In',
  ok: 'Ok',
  install: 'Install',
};

export const pagesUrl = {
  index: '/',
  app: '/',
  home: '/',
  installProcess: '/install-process',
  clusterName: '/cluster-name',
  cloudProvider: '/cloud-provider',
  authorization: '/authorization',
  clusterSpecs: '/cluster-specs',
  solutionBlueprint: '/solution-blueprint',
  summaryInformation: '/summary-information',
  install: '/install',
  login: '/login',
};
