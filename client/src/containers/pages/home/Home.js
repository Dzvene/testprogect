/* eslint-disable complexity */
import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { AutoSizer, Table, Column, SortDirection } from 'react-virtualized';

import { fetchData } from '../../../redux/actions/home';

const spinner = require('./../../../assets/images/spinner.svg'); // eslint-disable-line

import './styles.less';

const propTypes = {
  fetchData: PropTypes.func,
};

const ZERO = 0;

class Home extends PureComponent {
  state = {
    data: [],
    showSpinner: true,
  };

  sizes = [];

  componentDidMount() {
    this.props.fetchData();
  }

  mathColumnWidth = (isLast, last, width, widthAllColumns) => {
    if (!isLast) {
      return this.sizes[last] || 300;
    }

    return (width > widthAllColumns ? this.sizes[last] + (width - widthAllColumns) : this.sizes[last]);
  };

  render() {
    const rows = this.props.data;

    this.sortedRows = rows;

    const rowGetter = ({ index }) => rows && rows.length && this.sortedRows[index];
    const widthAllColumns = this.sizes.reduce((prev, next) => prev + next, ZERO);

    console.log(rows);

    return (
      <div
        className={'home'}
        style={{ height: 'calc(100% - 65px)' }}
      >
        <div
          className={'content home-table'}
          style={{ height: 'calc(100% - 110px)' }}
        >
          {/*<AutoSizer>*/}
            {/*{({ width, height }) => (*/}
              {/*<Table*/}
                {/*headerHeight={56}*/}
                {/*height={height}*/}
                {/*rowCount={rows && rows.length || 1}*/}
                {/*rowGetter={rowGetter}*/}
                {/*rowHeight={40}*/}
                {/*style={{ backgroundColor: '#fff' }}*/}
                {/*width={width}*/}
              {/*>*/}
                {/*{rows.length >= 1 && rows.map((key, i, list) =>*/}
                    {/*<Column*/}
                      {/*dataKey={key}*/}
                      {/*disableSort={false}*/}
                      {/*key={`${key}${i}`}*/}
                      {/*width={this.mathColumnWidth(i === list.length - 1, i, width, widthAllColumns)}*/}
                    {/*/>*/}
                {/*)}*/}
              {/*</Table>*/}
            {/*)}*/}
          {/*</AutoSizer>*/}

          {this.state.showSpinner ? <img src={spinner} style={{padding: 40}}/> : 'No data!'}
        </div>
      </div>
    );
  }
}

Home.propTypes = propTypes;

const mapStateToProps = (state) => ({
  data: state.homeReducer.data,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: () => dispatch(fetchData())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

