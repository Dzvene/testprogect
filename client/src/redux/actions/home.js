import axios from 'axios';
import { baseURL } from '../../constants/API';

export const GET_DATA = 'GET_DATA';

export const fetchData = () => {

  axios.get(baseURL).
    then(({ data }) => {
      return ({
        error: '',
        type: GET_DATA,
        data: data
      });
    }).
    catch((data) => {
      console.error(`Error response! ${data}`);
      return ({
        error: `Error response! ${data}`,
        type: GET_DATA,
        data: []
      });
  });
};
