import { GET_DATA } from '../actions/home';

const initialState = {
  data: [],
  error: ''
};

const homeReducer = (state = initialState, action) => {

  console.log(action.type);
  console.log(action.error);

  switch (action.type) {
    case GET_DATA:
      const data = action.data;
      const error = action.error;
      return { ...state, data, error};
    default:
      return state;
  }
};

export default homeReducer;
