/* eslint-disable no-underscore-dangle */

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import persistState from 'redux-localstorage'
import rootReducer from './reducers';

const getMiddleware = () => {
  const middleware = [
    thunk,
  ];

  return applyMiddleware(...middleware);
};

export default function configureStore(initialState) {
  const store = compose(
    getMiddleware(),
    persistState(),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  )(createStore)(rootReducer, initialState);

  return store;
}
