const Data = require('../models/data.model.js');

exports.create = function(req, res) {
    const request = req.body;

    if(!req.body) {
        res.status(400).send({message: "Note can not be empty"});
    }

    const data = new Data({
        title: request.title || 'No title',
        short_description: request.short_description || 'No description',
        full_description: request.full_description || 'No full description',
        date_created: request.data_created || 'No data created',
        last_update: request.last_update || 'No data last update',
        content: request.content || 'No content',
        cost: request.cost || 0,
        sell: request.sell || 0,
        count_views: request.count_views || 0,
    });

    data.save(function(err, data) {
        console.log(data);
        if(err) {
            console.log(err);
            res.status(500).send({message: "Some error ocuured while creating the Data."});
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Data.find(function(err, data){
        if(err) {
            res.status(500).send({message: "Some error ocuured while retrieving data."});
        } else {
            res.send(data);
        }
    });
};

exports.findOne = function(req, res) {
    Data.findById(req.params.dataId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not retrieve note with id " + req.params.dataId});
        } else {
            res.send(data);
        }
    });
};

exports.update = function(req, res) {
    Data.findById(req.params.dataId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not find a note with id " + req.params.dataId});
        }

        data.title = req.body.title;
        data.content = req.body.content;

        data.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Could not update note with id " + req.params.dataId});
            } else {
                res.send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    Data.remove({_id: req.params.dataId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not delete note with id " + req.params.id});
        } else {
            res.send({message: "Note deleted successfully!"})
        }
    });
};
