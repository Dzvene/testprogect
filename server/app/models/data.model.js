const mongoose = require('mongoose');

const DataSchema = mongoose.Schema({
    title: String,
    short_description: String,
    full_description: String,
    data_created: String,
    last_update: String,
    content: String,
    cost: Number,
    sell: Number,
    count_views: Number,
}, {
    timestamps: true
});

module.exports = mongoose.model('Data', DataSchema);
