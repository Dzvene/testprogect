const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const url = 'mongodb://localhost:27017/big-data';

const app = express();
const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./app/routes/data.routes.js')(app);


mongoose.connect(url, {
    useMongoClient: true
});

mongoose.connection.once('open', function() {
    console.log("Successfully connected to the database");
});

mongoose.connection.on('error', function() {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

app.listen(port, function(){
    console.log(`Server is listening on port ${port}`);
});

app.get('/', function(req, res){
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

